﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerscripts : MonoBehaviour
{
    CharacterController Canon;

    public float speed = 10;
    public float jumpspeed = 10;
    public float gravity = -9.81f;
    public Vector3 move;
    public Vector3 velocity;
    // Start is called before the first frame update
    void Start()
    {
        Canon = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        Canon.Move(move * speed * Time.deltaTime);

        if(move != Vector3.zero)
        {
            transform.forward = move;
        }

        velocity.y += gravity   * Time.deltaTime;

        Canon.Move(velocity * Time.deltaTime);

        if(Canon.isGrounded && velocity.y < 0)
        {
            velocity.y = 0.0f;
        }

        if(Input.GetButtonDown("Jump") && Canon.isGrounded)
        {
            velocity.y += jumpspeed;
        }
    }
}
