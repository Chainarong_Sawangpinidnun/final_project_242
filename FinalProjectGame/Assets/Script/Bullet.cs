﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Singleton<Bullet>
{

    [SerializeField] private float damageBullet;
    [SerializeField] private float interval;
    private float healthEnermy;
    
    void Start()
    {
        healthEnermy = GameObject.FindGameObjectWithTag("Enermy").layer;
    }
   
    public void OnCollisionStay(Collision Other)
    {
        Destroy(gameObject);

    }


    private void Update()
    {

        if(interval > 0)
        {
            interval -= Time.deltaTime;
        }
        else
        {
            enabled = false;
            Destroy(gameObject);
        }
    }

}
